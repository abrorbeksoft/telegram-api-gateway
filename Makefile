CURRENT_DIR=$(shell pwd)

proto-gen:
		./scripts/proto-gen.sh ${CURRENT_DIR}

start:
	go run cmd/main.go

swag-init:
		swag init -g api/main.go -o api/docs

.PHONY:compile
