package main

import (
	"gitlab.com/abrorbeksoft/telegram-api-gateway/api"
	"gitlab.com/abrorbeksoft/telegram-api-gateway/config"
	"gitlab.com/abrorbeksoft/telegram-api-gateway/service"
)

func main()  {
	cfg:=config.Load()
	grpcClients := service.NewGrpcClients(&cfg)

	router:=api.New(&api.RouterOptions{
		Services: grpcClients,
	})

	router.Run(":8080")
}

