package config

import (
	"github.com/joho/godotenv"
	"github.com/spf13/cast"
	"log"
	"os"
)

type Config struct {
	TelegramServicePort string
	TelegramServiceHost string
}

func Load() Config {
	if err:=godotenv.Load(); err != nil {
		log.Panic(err.Error())
	}
	conf := Config{}
	conf.TelegramServicePort = cast.ToString(getOrReturnDefault("TELEGRAM_SERVICE_PORT",""))
	conf.TelegramServiceHost = cast.ToString(getOrReturnDefault("TELEGRAM_SERVICE_HOST",""))
	return conf
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	val, exists := os.LookupEnv(key)
	if exists {
		return val
	}

	return defaultValue
}
