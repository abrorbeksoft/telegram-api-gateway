package api

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/swaggo/files"                                 // swagger embed files // 2
	"github.com/swaggo/gin-swagger"                           // gin-swagger middleware
	_ "gitlab.com/abrorbeksoft/telegram-api-gateway/api/docs" // 4
	v1 "gitlab.com/abrorbeksoft/telegram-api-gateway/api/handlers/v1"
	"gitlab.com/abrorbeksoft/telegram-api-gateway/api/messagetimer"
	"gitlab.com/abrorbeksoft/telegram-api-gateway/service"
	"net/http"
	"time"
)

type RouterOptions struct {
	Services service.ServiceManager
}

// @title Swagger Example API
// @version 1.0
// @description This is a sample server celler server.
// @termsOfService http://swagger.io/terms/
func New(options *RouterOptions) *gin.Engine {
	router:=gin.New()
	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	//var rateLimiter RateLimiter
	//router.Use(rateLimiterMiddleware(rateLimiter, 30))

	handlerV1:=v1.NewHandlerV1(v1.HandlerV1Options{
		Services: options.Services,
		MessageTimer: messagetimer.NewContainer(),
	})


	routerV1:= router.Group("/v1")
	routerV1.Use()
	{
		routerV1.GET("/", func(context *gin.Context) {
			context.JSON(http.StatusOK, gin.H{
				"message": "Server is running",
			})
		})
		routerV1.GET("/allChats",handlerV1.GetAllReceivers)
		routerV1.POST("/sendTest",handlerV1.SendText)
		routerV1.POST("/sendMessage",handlerV1.BindQueue)
		go handlerV1.SendEveryGivenSeconds(10)
	}
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler)) // 3

	return router
}




func rateLimiterMiddleware(limiter RateLimiter, delay float64) gin.HandlerFunc {
	return func(context *gin.Context) {
		clientIp:=context.ClientIP()
		resp,index:=limiter.Search(clientIp)
		now:=time.Now()
		if resp == nil {
			limiter.Push(clientIp)
			context.Next()
		} else {
			diff := now.Sub(resp.RequestTime)
			if diff.Seconds() >= delay {
				context.Next()
				limiter.Requests[index].RequestTime=time.Now()
			} else {
				context.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
					"message": fmt.Sprintf("Yu have to wait"),
				})
			}
		}
	}
}
