package models

type TextMessage struct {
	Receiver string `json:"receiver"`
	Text string `json:"text"`
	Priority string `json:"priority"`
}

type Receiver struct {
	Id string `json:"id"`
	Type string `json:"type"`
	Title string `json:"title"`
}

type Response struct {
	TextResponse string `json:"message"`
	Status int `json:"status"`
}

type Message struct {
	Text string `json:"text"`
	Priority string `json:"priority"`
}