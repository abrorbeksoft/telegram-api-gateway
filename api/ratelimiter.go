package api

import (
	"errors"
	"sort"
	"time"
)

type RateLimiter struct {
	Requests []Request
}

type Request struct {
	RequestTime time.Time `json:"last_request_time"`
	RequestIp string `json:"ip"`
}

func (rl *RateLimiter) Sort()  {
	sort.Slice(rl.Requests, func(i, j int) bool {
		return rl.Requests[i].RequestIp<rl.Requests[j].RequestIp
	})
}

func (rl *RateLimiter) Push(ip string)  {
	rl.Requests = append(rl.Requests, Request{
		RequestTime: time.Now(),
		RequestIp: ip,
	})
	rl.Sort()
}

func (rl *RateLimiter) Search(ip string) (*Request,int) {
	resp,index, err:=rl.QuickSearch(ip,0, len(rl.Requests) - 1)
	if err!=nil {
		return nil,index
	}
	return resp,index
}

//func (rl *RateLimiter) Update(request *Request) error {
//
//}

func (rl *RateLimiter) QuickSearch(ip string, left , right int) (*Request, int, error)  {
	if left<=right {
		midd:=(right+left)/2
		if rl.Requests[midd].RequestIp == ip {
			return &rl.Requests[midd], midd, nil
		}

		if ip > rl.Requests[midd].RequestIp {
			return rl.QuickSearch(ip, midd+1, right)
		} else {
			return rl.QuickSearch(ip, 0, midd-1)
		}
	}
	return nil, -1, errors.New("Not found!")
}
