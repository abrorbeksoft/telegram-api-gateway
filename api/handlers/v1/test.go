package v1

import (
	"context"
	"github.com/gin-gonic/gin"
	telegram "gitlab.com/abrorbeksoft/telegram-api-gateway/genproto/telegram_service"
	"log"
	"net/http"
)

func (h *handlerV1) Hello(c *gin.Context)  {
	resp, err:=h.services.Telegram().Send(context.Background(), &telegram.SendMessage{
		Receiver: "-1001709967327",
		Message:  &telegram.Message{
			Text: "How are you",
			Priority: "low",
		},
	})
	if err!=nil {
		log.Panic(err.Error())
	}

	c.JSON(http.StatusOK, resp)
}
