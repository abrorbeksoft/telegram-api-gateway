package v1

import (
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/abrorbeksoft/telegram-api-gateway/api/models"
	telegram "gitlab.com/abrorbeksoft/telegram-api-gateway/genproto/telegram_service"
	"log"
	"net/http"
	"time"
)

// @Router /v1/sendTest [POST]
// @Tags Telegram
// @Summary Sends message to telegram
// @Description Sends message to telegram
// @ID sends-message-to-telegram
// @Param message body models.TextMessage true "Message that to be send"
// @Accept  json
// @Produce  json
// @Success 200 {object} models.Response
// @Failure 400,404 {object} models.Response
func (h *handlerV1) SendText(c *gin.Context)  {
	var message models.TextMessage
	if err:=c.BindJSON(&message); err != nil {
		h.SendBadRequest(c, err.Error())
		return
	}

	resp, err:=h.services.Telegram().Send(context.Background(), &telegram.SendMessage{
		Receiver: message.Receiver,
		Message:  &telegram.Message{
			Text: message.Text,
			Priority: message.Priority,
		},
	})

	if err!=nil {
		h.SendBadRequest(c, err.Error())
		return
	}

	c.JSON(http.StatusOK, resp)
}

// @Router /v1/allChats [GET]
// @Tags Telegram
// @Summary Gets receivers list
// @Description Get all available receivers
// @ID get-all-receivers
// @Produce  json
// @Success 200 {array} models.Receiver
// @Failure 400,404 {object} models.Response
func (h *handlerV1) GetAllReceivers(c *gin.Context)  {
	resp, err:=h.services.Telegram().GetChats(context.TODO(),&telegram.EmptyRequest{})
	if err!=nil {
		h.SendBadRequest(c, err.Error())
		return
	}
	c.JSON(http.StatusOK, resp.Chats)
}


// @Router /v1/sendMessage [POST]
// @Tags Telegram
// @Summary Binds messages to queue
// @Description Binds messages to queue
// @ID bind-message-to-queue
// @Param message body models.TextMessage true "Message that to be send"
// @Accept  json
// @Produce  json
// @Success 200 {object} models.Response
// @Failure 400,404 {object} models.Response
func (h *handlerV1) BindQueue(c *gin.Context)  {
	var message models.TextMessage
	err:=c.BindJSON(&message)
	if err!=nil{
		h.SendBadRequest(c, err.Error())
		return
	}
	switch message.Priority {
	case "low":
		h.messageTimer.PushBackLow(&models.TextMessage{
		Receiver: message.Receiver,
		Priority: message.Priority,
		Text: message.Text,
	})
	case "medium" :
		h.messageTimer.PushBackMedium(&models.TextMessage{
		Receiver: message.Receiver,
		Priority: message.Priority,
		Text: message.Text,
	})
	case "high" :
		h.messageTimer.PushBackHigh(&models.TextMessage{
		Receiver: message.Receiver,
		Priority: message.Priority,
		Text: message.Text,
	})
	default:
		h.SendBadRequest(c, "Bunday status mavjud emas")
		return
	}

	c.JSON(http.StatusOK, models.Response{
		TextResponse: "Message was send",
		Status:       200,
	})
}


func (h *handlerV1) SendEveryGivenSeconds(period time.Duration)  {
	for true {
		time.Sleep(period * time.Second)
		if h.messageTimer.LengthHigh() > 0 {
			msg,_:=h.messageTimer.FrontHigh()
			newTextMsg:=msg.Value.(*models.TextMessage)

			resp,err:=h.services.Telegram().Send(context.TODO(), &telegram.SendMessage{
				Receiver: newTextMsg.Receiver,
				Message: &telegram.Message{
					Priority: newTextMsg.Priority,
					Text: newTextMsg.Text,
				},
			})
			log.Println(resp.Text)
			if err!=nil {
				log.Println(err.Error())
			}
			continue
		}

		if h.messageTimer.LengthMedium() > 0 {
			msg,_:=h.messageTimer.FrontMedium()
			newTextMsg:=msg.Value.(*models.TextMessage)
			resp,err:=h.services.Telegram().Send(context.TODO(), &telegram.SendMessage{
				Receiver: newTextMsg.Receiver,
				Message: &telegram.Message{
					Priority: newTextMsg.Priority,
					Text: newTextMsg.Text,
				},
			})
			log.Println(resp.Text)
			if err!=nil {
				log.Println(err.Error())
			}
			continue
		}

		if h.messageTimer.LengthLow() > 0 {
			msg,_:=h.messageTimer.FrontLow()
			newTextMsg:=msg.Value.(*models.TextMessage)
			resp,err:=h.services.Telegram().Send(context.TODO(), &telegram.SendMessage{
				Receiver: newTextMsg.Receiver,
				Message: &telegram.Message{
					Priority: newTextMsg.Priority,
					Text: newTextMsg.Text,
				},
			})
			log.Println(resp.Text)
			if err!=nil {
				log.Println(err.Error())
			}
			continue
		}
		fmt.Println("Queue is null")
	}
}