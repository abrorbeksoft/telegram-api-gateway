package v1

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/abrorbeksoft/telegram-api-gateway/api/messagetimer"
	"gitlab.com/abrorbeksoft/telegram-api-gateway/service"
	"net/http"
)

type handlerV1 struct {
	services service.ServiceManager
	messageTimer *messagetimer.MessageContainer
}

type HandlerV1Options struct {
	Services service.ServiceManager
	MessageTimer *messagetimer.MessageContainer
}

func NewHandlerV1(options HandlerV1Options) *handlerV1 {
	return &handlerV1{
		services: options.Services,
		messageTimer: options.MessageTimer,
	}
}

func (h *handlerV1) SendBadRequest(c *gin.Context, message string)  {
	c.JSON(http.StatusBadRequest, message)
}

