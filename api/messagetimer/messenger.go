package messagetimer

import (
	"container/list"
	"errors"
	"gitlab.com/abrorbeksoft/telegram-api-gateway/api/models"
)



type MessageContainer struct {
	low *list.List
	medium *list.List
	high *list.List
}

func NewContainer() *MessageContainer {
	return &MessageContainer{
		low: list.New(),
		medium: list.New(),
		high: list.New(),
	}
}

func (mc *MessageContainer) PushBackLow(message *models.TextMessage)  {
	mc.low.PushBack(message)
}

func (mc *MessageContainer) PushBackMedium(message *models.TextMessage)  {
	mc.medium.PushBack(message)
}

func (mc *MessageContainer) PushBackHigh(message *models.TextMessage)  {
	mc.high.PushBack(message)
}

func (mc *MessageContainer) LengthLow() int {
	return mc.low.Len()
}

func (mc *MessageContainer) LengthMedium() int {
	return mc.medium.Len()
}

func (mc *MessageContainer) LengthHigh() int {
	return mc.high.Len()
}


func (mc *MessageContainer) FrontLow() (*list.Element,error)  {
	if mc.low.Len() <= 0 {
		return nil, errors.New("Queue is empty!")
	}
	front:=mc.low.Front()
	mc.low.Remove(front)
	return front,nil
}

func (mc *MessageContainer) FrontMedium() (*list.Element,error) {
	if mc.medium.Len() <= 0 {
		return nil, errors.New("Queue is empty!")
	}
	front:=mc.medium.Front()
	mc.medium.Remove(front)
	return front,nil
}

func (mc *MessageContainer) FrontHigh() (*list.Element,error) {
	if mc.high.Len() <= 0 {
		return nil, errors.New("Queue is empty!")
	}
	front:=mc.high.Front()
	mc.high.Remove(front)
	return front,nil
}