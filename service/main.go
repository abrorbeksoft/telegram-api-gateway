package service

import (
	"fmt"
	"gitlab.com/abrorbeksoft/telegram-api-gateway/config"
	"gitlab.com/abrorbeksoft/telegram-api-gateway/genproto/telegram_service"
	"google.golang.org/grpc"
	"log"
)

type ServiceManager interface {
	Telegram() telegram_service.TelegramServiceClient
}

type grpcClients struct {
	telegram telegram_service.TelegramServiceClient
}

func NewGrpcClients(cfg *config.Config) ServiceManager {

	connTelegramService, err := grpc.Dial(fmt.Sprintf("%s:%s",cfg.TelegramServiceHost,cfg.TelegramServicePort),grpc.WithInsecure())
	if err!=nil {
		log.Panic(err.Error())
	}

	return &grpcClients{
		telegram: telegram_service.NewTelegramServiceClient(connTelegramService),
	}
}

func (g *grpcClients) Telegram() telegram_service.TelegramServiceClient {
	return g.telegram
}

